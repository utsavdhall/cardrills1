function problem2(inventory){
    if(inventory==undefined || inventory==[] || input==undefined){
        return [];
    }
    if(inventory.isArray()==false){
        return [];
    }
    let last=inventory.length-1;
    return `Last car is a ${inventory[last]["car_make"]} ${inventory[last]["car_model"]}`;
}
module.exports=problem2;