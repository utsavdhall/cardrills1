function problem4(inventory){
    if(inventory==undefined || inventory==[] || input==undefined){
        return [];
    }
    if(inventory.isArray()==false){
        return [];
    }
    let n=inventory.length;
    let years=new Array(n).fill("");
    for(let index=0;index<n;index++){
        years[index]=inventory[index]["car_year"];
    }
    return years;
}
module.exports=problem4;