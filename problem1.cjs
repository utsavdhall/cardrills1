function problem1(inventory,input){
    if(inventory==undefined || inventory==[] || input==undefined){
        return [];
    }
    if(Array.isArray(inventory)==false){
        return [];
    }
    for(let index=0;index<inventory.length;index++){
        if(inventory[index]["id"]==input){
            return inventory[index];
        }
    }
    return [];
}
module.exports=problem1;