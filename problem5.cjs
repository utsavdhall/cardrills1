const problem4= require("./problem4.js");
function problem5(inventory){
    if(inventory==undefined || inventory==[] || input==undefined){
        return [];
    }
    if(inventory.isArray()==false){
        return [];
    }
    let years=problem4(inventory);
    let n=years.length;
    let yearsLessThan2000=[]
    for(let index=0;index<n;index++){
        if(years[index]<2000){
            yearsLessThan2000.push(years[index]);
        }
    }
    return yearsLessThan2000.length;

}
module.exports=problem5;